<?php

        if (! $banner_color)
                $banner_color = "#444";
        if (! $bgcolor)
                $bgcolor = "#0a0a0a";
        if (! $overlaybg)
                $overlaybg = "#222";
        if (! $subtle_shade)
                $subtle_shade = "#333";
        if (! $mid_shade)
                $mid_shade = "#aaa";
        if (! $contrast_shade)
                $contrast_shade = '#c4c4bc';
        if (! $font_color)
                $font_color = "#dadacd";
        if (! $primary)
                $primary = "#72c2d5";
        if (! $secondary)
                $secondary = "#217589";
        if (! $success)
                $success = "#37cfa9";
        if (! $link_color)
                $link_color = "#72c2d5";
        if (! $link_hover)
                $link_hover = "#217589";
        if (! $font_contrast_color)
                $font_contrast_color = "#000";
        if (! $border)
                $border = "1px solid #000";
        if (! $shadow)
                $shadow = "0 1px 4px #000";                
   
