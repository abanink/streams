<?php

namespace Code\Lib;


use Code\ActivityStreams\UnhandledElementException;

class BaseObject
{

    /**
     * @param $input
     * @param $strict
     * @throws UnhandledElementException if $strict
     */

    public function __construct($input = null, $strict = false)
    {
        if (isset($input) && is_array($input)) {
            foreach ($input as $key => $value) {
                if ($strict && !property_exists($this, $key)) {
                    throw new UnhandledElementException("Unhandled element: $key");
                }
                $this->{$key} = $value;
            }
        }
        return $this;
    }

    public function getDataType($element, $object = null)
    {
        if (is_null($object)) {
            $object = $this;
        }

        if (!isset($object[$element])) {
            return 'null';
        }
        if (is_string($object[$element])) {
            return 'string';
        }
        if (is_array($object[$element])) {
            if (array_key_exists(0, $object[$element])
                || empty($object[$element])) {
                return 'array';
            } else {
                return 'object';
            }
        }
        return 'other';
    }


    public function toArray()
    {
        $returnValue = [];
        foreach ((array)$this as $key => $value) {
            if (isset($value)) {
                $key = ($key === 'ldcontext') ? '@context' : $key;
                $returnValue[$key] = $value;
            }
        }
        return $returnValue;
    }
}
